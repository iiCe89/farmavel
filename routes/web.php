<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/game', function () {
    return view('game');
});

Route::get('/leaderboard', function () {
    $leaderboard = cache()->remember('leaderboard', 15, function () {
        $leaderboardCollection = App\Leaderboard::get();

        $leaderboard = [];

        foreach ($leaderboardCollection->sortByDesc('money') as $leaderboardItem) {
            if (array_key_exists($leaderboardItem->user_id, $leaderboard)) {
                if ($leaderboard[$leaderboardItem->user_id]['money'] <= $leaderboardItem->money) {
                    $leaderboard[$leaderboardItem->user_id] = collect([
                        'user_name' => $leaderboardItem->user_name,
                        'money' => $leaderboardItem->money,
                    ]);
                }
            } else {
                $leaderboard[$leaderboardItem->user_id] = collect([
                    'user_name' => $leaderboardItem->user_name,
                    'money' => $leaderboardItem->money,
                ]);
            }
        }

        return collect($leaderboard);
    });

    return view('leaderboard', compact('leaderboard'));
});

Route::post('/save-game', function () {
    if (auth()->check()) {
        \App\Leaderboard::create(request()->except('_token'));
        return response('success');
    }

    return response('Login!!', 404);
});
