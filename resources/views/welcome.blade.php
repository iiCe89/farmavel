@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to farmavel</div>

                <div class="panel-body">
                    Farmavel is a game where you manage your own farm while producing items.<br>
                    Each level you get more possibilities and you can make more money.<br>
                    You can play the game for free and your progress will be stored.<br>
                    When you register, you will appear in the player boards and you can strive to be the top farmer in farmavel!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
