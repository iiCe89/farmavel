<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOilFieldsToLeaderboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaderboards', function (Blueprint $table) {
            $table->unsignedInteger('oil')->after('windmill_grind_capacity')->nullable();
            $table->unsignedInteger('barrels')->after('oil')->nullable();
            $table->float('barrel_price')->after('barrels')->nullable();
            $table->unsignedInteger('barrel_capacity')->after('barrel_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaderboards', function (Blueprint $table) {
            $table->dropColumn(['oil', 'barrels', 'barrel_price', 'barrel_capacity']);
        });
    }
}
