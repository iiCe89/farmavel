<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaderboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaderboards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('user_name')->nullable();
            $table->unsignedInteger('level')->nullable();
            $table->unsignedInteger('next_level')->nullable();
            $table->float('money')->nullable();
            $table->unsignedInteger('farmlands')->nullable();
            $table->string('farmland_yield')->nullable();
            $table->float('farmland_price')->nullable();
            $table->unsignedInteger('grain')->nullable();
            $table->unsignedInteger('grain_silo_capacity')->nullable();
            $table->unsignedInteger('grain_silos')->nullable();
            $table->float('grain_silo_price')->nullable();
            $table->unsignedInteger('flour')->nullable();
            $table->unsignedInteger('flour_silos')->nullable();
            $table->unsignedInteger('flour_silo_capacity')->nullable();
            $table->float('flour_silo_price')->nullable();
            $table->unsignedInteger('windmills')->nullable();
            $table->float('windmill_price')->nullable();
            $table->unsignedInteger('windmill_grind_capacity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaderboards');
    }
}
